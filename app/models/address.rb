class Address < ActiveRecord::Base
  belongs_to :hotel

  validates :country, presence: true, length: { maximum: 128 }
  validates :city, presence: true, length: { maximum: 64 }
  validates :street, presence: true, length: { maximum: 64 }
  validates :corp, presence: true, length: { maximum: 8 }
end
