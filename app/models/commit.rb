class Commit < ActiveRecord::Base
  belongs_to :user
  belongs_to :hotels

  validates :comment, length: { maximum: 512 }
  validates_inclusion_of :star_rating, :in => 1..5

end
