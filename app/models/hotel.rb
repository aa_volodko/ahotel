class Hotel < ActiveRecord::Base
  belongs_to :user
  has_many :commits
  has_one :address
  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :commits
  mount_uploader :photo, PhotoUploader

  validates :title, presence: true, length: { maximum: 128 }
  validates :star,
            :numericality => { :greater_than_or_equal_to => 0.00,
                               :less_than_or_equal_to => 5.00 }
  validates :room, presence: true, length: { maximum: 1000 }
  validates :price, presence: true,
            :numericality => { :greater_than_or_equal_to => 3.00,
            	                 :less_than_or_equal_to => 50000.00 }

                            
end
