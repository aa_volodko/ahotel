module ApplicationHelper

  def sortable_hotel(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_hotel_column ? "current #{sort_direction}" : nil
    direction = column == sort_hotel_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, params.merge(:sort => column, :direction => direction, :page => nil), {:class => css_class}
  end

  def sortable_user(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_user_column ? "current #{sort_direction}" : nil
    direction = column == sort_user_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, params.merge(:sort => column, :direction => direction, :page => nil), {:class => css_class}
  end

end
