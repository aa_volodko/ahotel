class CommitsController < ApplicationController
  before_filter :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]

  def create
    @hotel = Hotel.find(cookies[:last_hotel_id])
    @commit = @hotel.commits.new(commit_params)
    @commit.hotel_id = @hotel.id
    @commit.user_id = current_user.id
    if @commit.save
      flash[:success] = "Success!"
      redirect_to @hotel
    else
      redirect_to root_path
      flash[:alert] = "Ooops!"
    end

  end

  private
    def commit_params
      params.require(:commit).permit(:star_rating, :comment)
    end

end
