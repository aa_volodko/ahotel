class StaticPagesController < ApplicationController
  respond_to :html, :xml, :json
  def home
    @hotels = Hotel.where(approved: 'true').order('star DESC').limit(5)
    @users = User.all
    respond_with @hotel
  end

  def help
  end

  def about
  end
end
