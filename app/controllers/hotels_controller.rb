class HotelsController < ApplicationController
  respond_to :html, :xml, :json
  before_action :pick_hotel, only: [:show, :edit, :update, :destroy]

  def show
    @address = Address.where(hotel_id:(@hotel.id))
    @commits = Commit.where(hotel_id: params[:id])
    respond_with @hotel
    cookies[:last_hotel_id] = @hotel.id
  end

  def new
    @hotel = Hotel.new
    @address = @hotel.build_address
    respond_with @hotel
  end

  def create
    @hotel = Hotel.new(hotel_params)
    @hotel.user_id = current_user.id
    if @hotel.save
      UserMailer.create_hotel_email(current_user).deliver
      redirect_to @hotel
      flash[:success] = "Success!"
    else
      flash[:alert] = "Ooops!"
      render 'new'
    end
  end

  def edit
  end

   def update
    if @hotel.update_attributes(hotel_params)
      if @hotel.approved = true
        UserMailer.approved_hotel_email(user).deliver
      end
      if @hotel.rejected = true
        UserMailer.rejected_hotel_email(user).deliver
      end

      redirect_to @hotel
    else
      render 'edit'
    end
  end

  def destroy
    @hotel.destroy
    redirect_to root_path
  end


    private
       def hotel_params
         params.require(:hotel).permit(
            :title,:star,:breakfast,:room,:photo,:price, :pending, :approved, :rejected,
            address_attributes: [:country, :state, :city, :street, :corp])
       end

       def pick_hotel
         @hotel = Hotel.find(params[:id])
       end
end
