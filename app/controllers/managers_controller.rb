class ManagersController < ApplicationController
  before_action :authenticate_manager!
  helper_method :sort_hotel_column, :sort_user_column, :sort_direction

  def index
    if params[:search]
      @users = User.search(params[:search]).order(sort_user_column => sort_direction).paginate(:per_page => 5, :page => params[:page])
    else
      @users = User.order(sort_user_column => sort_direction).paginate(:per_page => 5, :page => params[:page])
    end
  end

  def show
    if params[:search]
      @hotels = Hotel.search(params[:search]).order(sort_hotel_column => sort_direction).paginate(:per_page => 3, :page => params[:page])
    else
      @hotels = Hotel.all.order(sort_hotel_column => sort_direction).paginate(:per_page => 3, :page => params[:page])
    end

    if params[:approved].present?
      @hotels = Hotel.where(approved: true).order(sort_hotel_column => sort_direction).paginate(:per_page => 3, :page => params[:page])
    elsif params[:pending].present?
      @hotels = Hotel.where(pending: true).order(sort_hotel_column => sort_direction).paginate(:per_page => 3, :page => params[:page])
    elsif params[:rejected].present?
      @hotels = Hotel.where(rejected: true).order(sort_hotel_column => sort_direction).paginate(:per_page => 3, :page => params[:page])
    else
      @hotels = Hotel.all.order(sort_hotel_column => sort_direction).paginate(:per_page => 3, :page => params[:page])
    end
  end

  def User.search(search)
    if search
      where('email LIKE ?', "%#{search}%")
    else
      scoped
    end
  end

  def Hotel.search(search)
    if search
      where('title LIKE ?', "%#{search}%")
    else
      scoped
    end
  end


  private

    def sort_hotel_column
      Hotel.column_names.include?(params[:sort]) ? params[:sort] : "title"
    end

    def sort_user_column
      Hotel.column_names.include?(params[:sort]) ? params[:sort] : "email"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction].to_sym : :asc
    end

end
