ActiveAdmin.register Hotel do

  index do
    column :title
    column :star
    column "Hotel description", :room
    column :price, :sortable => :price do |hotel|
      div :class => "price" do
        number_to_currency hotel.price
      end
    end
    column "User_ID",:user_id
    column :created_at
  end

end
