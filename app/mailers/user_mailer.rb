class UserMailer < ActionMailer::Base
  default from: "admin@ahotel.com"

   def welcome_email(user)
    @recipient = user.email
    mail(:to => "#{user.email} <#{user.email}>", :subject => "Welcome to our Hotel Adviser")
  end

  def create_hotel_email(user)
    @recipient = user.email
    mail(:to => user.email, :subject => "You have just created a new hotel")
  end

  def approved_hotel_email(user)
    @recipient = user.email
    mail(:to => "#{user.email} <#{user.email}>", :subject => "Your hotel is approved")
  end

  def rejected_hotel_email(user)
    @recipient = user.email
    mail(:to => "#{user.email} <#{user.email}>", :subject => "Your hotel is rejected")
  end

end
