class AddPendingApprovedRejectedToHotel < ActiveRecord::Migration
  def change
    add_column :hotels, :pending, :boolean, default: true
    add_column :hotels, :approved, :boolean, default: false
    add_column :hotels, :rejected, :boolean, default: false
  end
end
