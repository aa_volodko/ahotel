class CreateCommits < ActiveRecord::Migration
  def change
    create_table :commits do |t|
      t.integer :star_rating
      t.text :comment
      t.integer :user_id
      t.integer :hotel_id

      t.timestamps
    end
    add_index :commits, [:user_id]
    add_index :commits, [:hotel_id]
  end
end
