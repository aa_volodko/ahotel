require "rails_helper"
RSpec.describe Address, :type => :model do

    let(:hotel) { FactoryGirl.create(:hotel) }
    before { @address = Address.create(country: "UK", state: "Some state",
                          city: "Manchester", street: "Main", corp: "3G",
                          hotel_id: hotel.id) }

      subject { @address }

    it { should respond_to(:country) }
    it { should respond_to(:state) }
    it { should respond_to(:city) }
    it { should respond_to(:street) }
    it { should respond_to(:corp) }
    it { should respond_to(:hotel_id) }

    it { should respond_to(:hotel) }
    it { should be_valid }


  describe "when country length is greater then longest in the world" do
    before { @address.country = "a" * 129 }
    it { should_not be_valid }
  end

  describe "when country is empty" do
    before { @address.country = "" }
    it { should_not be_valid }
  end

  describe "when city is to long" do
    before { @address.city = "a" * 65 }
    it { should_not be_valid }
  end

  describe "when city is empty" do
    before { @address.city = "" }
    it { should_not be_valid }
  end

  describe "when street is too long" do
    before { @address.street = "a" * 65 }
    it { should_not be_valid }
  end

  describe "when street is empty" do
    before { @address.street = "" }
    it { should_not be_valid }
  end

  describe "when corp is greater then 8 chars" do
    before { @address.corp = "a" * 9 }
    it { should_not be_valid }
  end

  describe "when corp is empty" do
    before { @address.corp = "" }
    it { should_not be_valid }
  end

end
