require "rails_helper"
RSpec.describe User, :type => :model do

     before do
       @user = User.new(email: "user@example.com",
                        password: "boozbaaz", password_confirmation: "boozbaaz")
     end

    subject { @user }

  it { should respond_to :email }
  it { should respond_to :password }
  it { should respond_to :password_confirmation }

  describe "when email is not present" do
    before { @user.email = " " }
    it { should_not be_valid }
  end

  describe "when password or confirmation is not present" do
    before { @user.password="" }
    it { should_not be_valid }
  end

  describe "when password is less than 6 char" do
    before { @user.password = @user.password_confirmation = "boozb" }
    it { should_not be_valid }
  end

  describe "when email format is invalid" do
    it "should be invalid" do
      addresses = %w[user@foo,com user_at_foo.org example.user@foo.
                  foo@bar_baz.com foo@bar+baz.com]
      addresses.each do |invalid_address|
      @user.email = invalid_address
      end
    end
  end

  describe "when password confirmation is empty" do
    before { @user.password_confirmation = "" }
    it { should_not be_valid }
  end

  describe "when email address is already taken" do
    before do
    user_with_same_email = @user.dup
    user_with_same_email.email = @user.email.upcase
    user_with_same_email.save
    end
    it { should_not be_valid }
  end

end
