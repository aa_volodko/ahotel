require "rails_helper"
RSpec.describe Hotel, :type => :model do

    let(:user) { FactoryGirl.create(:user) }
    let(:address) { FactoryGirl.create(:address) }
    before { @hotel = user.hotels.build(title: "New Hotel",star: 1,
                breakfast: false, room: "Very cheap", photo: "default_photo.png",
                price: 10) }

      subject { @hotel }

    it { should respond_to(:title) }
    it { should respond_to(:star) }
    it { should respond_to(:breakfast) }
    it { should respond_to(:room) }
    it { should respond_to(:photo) }
    it { should respond_to(:price) }
    it { should respond_to(:user_id) }

    it { should respond_to(:user) }
    it { should respond_to(:address) }
    it { should be_valid }


  describe "when title is empty" do
    before { @hotel.title = "" }
    it { should_not be_valid }
  end

  describe "when title is too long" do
    before { @hotel.title = "a" * 129 }
    it { should_not be_valid }
  end

  describe "when star is greater then 5" do
    before { @hotel.star = 6 }
    it { should_not be_valid }
  end

  describe "when star is empty" do
    before { @hotel.star = "" }
    it { should_not be_valid }
  end

  describe "when star is less then 0" do
    before { @hotel.star = -1 }
    it { should_not be_valid }
  end

  describe "when room description is too long" do
    before { @hotel.room = "a" * 1001 }
    it { should_not be_valid }
  end

  describe "when room description is empty" do
    before { @hotel.room = "" }
    it { should_not be_valid }
  end

  describe "when price is greater then highest in the world" do
    before { @hotel.price = 50001 }
    it { should_not be_valid }
  end

  describe "when price is empty" do
    before { @hotel.price = "" }
    it { should_not be_valid }
  end

  describe "when price is less then 0" do
    before { @hotel.price = -1 }
    it { should_not be_valid }
  end

end
