require "rails_helper"
RSpec.describe Commit, :type => :model do

    let(:user) { FactoryGirl.create(:user) }
    let(:hotel) { FactoryGirl.create(:hotel) }
    before { @commit = Commit.create(star_rating: 3, comment: "Comsi - comsa",
                          user_id: user.id, hotel_id: hotel.id) }

      subject { @commit }

    it { should respond_to(:star_rating) }
    it { should respond_to(:comment) }
    it { should respond_to(:user_id) }
    it { should respond_to(:hotel_id) }

    it { should respond_to(:user) }
    it { should be_valid }

  describe "when comment is too long" do
    before { @commit.comment = "a" * 513 }
    it { should_not be_valid }
  end

  describe "when star_rating is greater then 5" do
    before { @commit.star_rating = 6 }
    it { should_not be_valid }
  end

  describe "when star_rating is empty" do
    before { @commit.star_rating = "" }
    it { should_not be_valid }
  end

  describe "when star_rating is less OR equal then 0" do
    before { @commit.star_rating = 0 }
    it { should_not be_valid }
  end


end
