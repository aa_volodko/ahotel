FactoryGirl.define do
  factory :user do
      sequence(:email) { |n| "person_#{n}@example.com"}
      password "boozbaaz"
      password_confirmation "boozbaaz"
  end

  factory :hotel do
      title "Best Hotel"
      star 5
      breakfast true
      room "Splandid hotel, with..."
      photo "default_photo.png"
      price 150
      user
  end

  factory :address do
      country "Spain"
      state "Catalunya"
      city "Barselona"
      street "Main"
      corp "2A"
      hotel
  end

  factory :commit do
    star_rating 4
    comment "Splandid hotel, with..."
    hotel
  end

end
